<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DiscController@index')->name('index');

Auth::routes();

Route::get('/list', 'DiscController@discList')->name('list');

Route::get('/edit/{id}', 'DiscController@edit')->name('edit');

Route::post('/save', 'DiscController@save')->name('save');

Route::delete('/delete/{id}', 'DiscController@delete')->name('delete');
