<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;


class DiscController extends Controller
{
    const TABLE_NAME = 'discs';
    const DISC_ON_PAGE = 4;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::guest()) return view('welcome');

        return redirect('/list');
    }

    /**
     * Show a list of discs.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function discList()
    {
        $discs = DB::table(self::TABLE_NAME)
            ->where('user', Auth::id())
            ->paginate(self::DISC_ON_PAGE);
        return view('list', ['discs' => $discs]);
    }

    /**
     * Edit disc
     *
     * @param $id
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function edit($id)
    {
        $record = DB::table(self::TABLE_NAME)
            ->where('id', $id)
            ->where('user', Auth::id())
            ->get();

        if (count($record)) return view('edit', ['record' => $record[0]]);

        return view('404');
    }

    /**
     * Update disc
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function save(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|int|max:255',
            'title' => 'required|max:255',
            'date' => 'date',
            'author' => 'max:255',
            'user' => 'required|int|max:255'
        ]);

        DB::table(self::TABLE_NAME)
            ->where('id', $request->post('id'))
            ->where('user', Auth::id())
            ->update([
                'title' => $request->post('title'),
                'desc' => $request->post('desc'),
                'date' => $request->post('date'),
                'author' => $request->post('author')
            ]);

        return redirect('/list');
    }

    /**
     * Delete disc
     *
     * @param int $id disc
     */
    public function delete($id)
    {
        $result = DB::table(self::TABLE_NAME)
            ->where('id', $id)
            ->where('user', Auth::id())
            ->delete();
        if ($result) {
            echo Lang::get('main.success');
        } else {
            echo Lang::get('main.filed');
        }
    }
}
