<?php /** Created by Anton on 20.04.2019. */

return [
    'page-title' => 'Musical reference',
/*table*/
    //header
    'title' => 'Title',
    'desc' => 'Description',
    'date' => 'Date',
    'author' => 'Author',
    'action' => 'Action',
    //action
    'edit' => 'Edit',
    'delete' => 'Delete',
/*delete*/
    'js-msg' => 'Are you sure?',
    //status
    'success' => 'Record deleted',
    'filed' => 'Error. Record not deleted'
];