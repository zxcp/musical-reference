{{--Created by Anton on 20.04.2019.--}}

@extends('layouts.app')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('save') }}">
            @csrf

            @foreach($record as $key => $val)
                @if ($key == 'id' || $key == 'user')
                    <input type="number" class="form-control" name="{{ $key }}" value="{{ $val }}" required hidden>
                    @continue
                @endif
                <div class="form-group row">
                    <label for="{{ $key }}" class="col-md-4 col-form-label text-md-right">{{ ucfirst($key) }}</label>

                    <div class="col-md-6">
                        <input id="{{ $key }}" type="text" class="form-control{{ $errors->has($key) ? ' is-invalid' : '' }}" name="{{ $key }}" value="{{ $val }}" required>

                        @if ($errors->has($key))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $val }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            @endforeach
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4 text-center">
                    <button type="submit" class="btn btn-primary">
                        save
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
