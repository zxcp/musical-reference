@extends('layouts.app')

@section('content')
    <div class="container">
        <table class="table text-center">
            @csrf
            <thead>
                <tr>
                    <th>{{ trans('main.author') }}</th>
                    <th>{{ trans('main.title') }}</th>
                    <th>{{ trans('main.date') }}</th>
                    <th>{{ trans('main.desc') }}</th>
                    <th>{{ trans('main.action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($discs as $disc)
                    <tr>
                        <td class="text-nowrap">{{ $disc->author }}</td>
                        <td class="text-nowrap">{{ $disc->title }}</td>
                        <td class="text-nowrap">{{ (new DateTime($disc->date))->format('d.m.Y') }}</td>
                        {{--Чтобы описание не занимало много места выведено только 50 символов.
                            А полное описание выводится при наведении.--}}
                        <td title="{{ $disc->desc }}">{{ mb_substr($disc->desc, 0, 50) . '...' }}</td>
                        <td>
                            <a class="btn btn-success" href="{{ route('edit', ['id' => $disc->id]) }}">{{ trans('main.edit') }}</a>
                            <button class="btn btn-danger js-del" link="/delete/{{ $disc->id }}/">{{ trans('main.delete') }}</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-2 offset-5"><?php echo $discs->render(); ?></div>
        </div>
        <div id="js-msg" hidden>{{ trans('main.js-msg') }}</div>
    </div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="js/delete.js"></script>
