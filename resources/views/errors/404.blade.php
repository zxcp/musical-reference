{{-- Created by Anton on 21.04.2019. --}}

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-2 text-center">
                <img src="/img/404.png" alt="404">
            </div>
        </div>
    </div>
@endsection
