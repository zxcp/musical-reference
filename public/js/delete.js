/** Created by Anton on 21.04.2019. */

$(document).ready(function () {
    $('button.js-del').on('click', function (e) {
        var result = window.confirm($('#js-msg').text());
        if (result === false) {
            e.preventDefault();
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log($('meta[name="csrf-token"]').attr('content'));
            $.ajax({
                url: this.getAttribute('link'),
                method: 'delete',
                data: {
                    token: $('input[name="_token"]').val()
                },
                success: function( result ) {
                    alert(result);
                    location.reload();
                }
            });
        }
    });
});
