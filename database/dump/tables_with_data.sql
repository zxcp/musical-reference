-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `discs`;
CREATE TABLE `discs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `desc` text,
  `date` date DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `user` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `discs` (`id`, `title`, `desc`, `date`, `author`, `user`) VALUES
(1,	'Химера',	'«Химе́ра» — восьмой студийный альбом группы «Ария», вышедший в апреле 2001 года. Последний альбом с вокалистом Валерием Кипеловым.',	'2004-04-12',	'Ария',	1),
(2,	'Герой асфальта',	'«Геро́й асфа́льта» — третий студийный альбом группы «Ария». Это первый альбом после раскола группы в начале 1987 года и первый с участием Виталия Дубинина, Сергея Маврина и Максима Удалова.',	'1987-09-04',	'Ария',	1),
(3,	'С кем ты?',	'«С кем ты?» — второй студийный альбом группы «Ария». Был записан в 1986 году и распространялся как магнитоальбом на магнитофонной кассете. На CD был издан только в 1994 году, в 2013 году был впервые издан на виниле.',	'1986-11-03',	'Ария',	1),
(4,	'Игра с огнём',	'«Игра с огнём» — четвёртый студийный альбом группы «Ария». Первый альбом группы с участием ударника Александра Манякина и без постоянного продюсера Виктора Векштейна.',	'2019-04-11',	'Ария',	1),
(5,	'Мания величия',	'Единственный альбом «Арии», включённый в сводку «100 магнитоальбомов советского рока» Александра Кушнира.',	'1985-10-31',	'Ария',	1),
(6,	'Лети на свет',	'«Ма́ния вели́чия» — дебютный музыкальный альбом группы «Ария». Альбом посвящается всем тем, кого не зацепили, не вдохновили и просто оказались не по душе Кипеловские \"Реки Времён\". Ведь можно же в России делать музыку без ненужных выкрутасов на гитаре, без псевдомистических (и отдающих банальным русским роком) текстов!',	'2005-10-27',	'Артерия',	1),
(7,	'2014',	'2014 — третий студийный альбом российской хеви-метал группы «Артерия», который вышел 12 мая 2014 года.',	'2019-05-12',	'Артерия',	1),
(8,	'Comatose',	'Comatose — шестой студийный альбом христианской рок-группы Skillet, выпущенный 3 октября 2006 года лейблами INO Records, Lava Records, Atlantic Records и Ardent Records. Он записывался при участии продюсера Брайна Ховенса на студии Chicago Recording Company в период с февраля по апрель 2006 года.',	'2006-10-03',	'Skillet',	2),
(9,	'Rise',	'Rise — восьмой студийный альбом христианской рок-группы Skillet, который был издан 25 июня 2013 года. Rise — первый концептуальный альбом группы, хотя по словам вокалиста и бас-гитариста Skillet Джона Купера и продюсера Говарда Бенсона, изначально не планировалось объединять его одной темой.',	'2013-06-25',	'Skillet',	2),
(10,	'Mutter',	'Mutter — третий студийный альбом группы Rammstein, вышел 2 апреля 2001 года. Альбом записывался в Германии, Франции, Швеции и Америке. Журнал Metal Hammer включил Mutter в 200 лучших рок-альбомов всех времён.',	'2019-04-02',	'Rammstein',	2);

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'root',	'zxcp@list.ru',	NULL,	'$2y$10$BMXUSAN5IslbWtPwPngvpO7v.dap186pGzEi7yFLMJwDqGiELa7QW',	'FeeFP1iWPHeexGMuHSb32KQNmVrZ8ZwzXnrLpnYAoRE0ZjnZCymIZxpV2328',	'2019-04-20 08:56:31',	'2019-04-20 08:56:31'),
(2,	'admin',	'zxcp1@list.ru',	NULL,	'$2y$10$A.gvUXdqzjZ0BxsyDa1mMuHYA2TnY9IGO/WvE4J1tT3EIFCLqKzPW',	NULL,	'2019-04-21 15:11:29',	'2019-04-21 15:11:29');

-- 2019-04-21 19:17:59
